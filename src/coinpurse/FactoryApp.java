package coinpurse;

public class FactoryApp {
	public static void main(String[] args) {
		MoneyFactory factory = MoneyFactory.getInstance();
		System.out.println( factory.createMoney("10") );
		System.out.println( factory.createMoney(50.0) );

	}
}
