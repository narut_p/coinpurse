package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;

/**
 * 
 * @author Narut Poovorakit
 * @version 17.02.2015
 *
 */
public class GreedyWithdraw implements WithdrawStrategy {

	/**
	 * @param amount
	 *            of the money. and list of the money in the purse.
	 * @return null if amount is less then zero and return the list that add a
	 *         money in the purse.
	 */
	public ArrayList withdraw(double amount, List<Valuable> money) {
		ArrayList<Valuable> purse = new ArrayList<Valuable>();
		ArrayList<Valuable> newPurse = new ArrayList<Valuable>();

		if (amount < 0) {

			return null;
		}
		if (amount > 0) {
			for (int i = 0; i < money.size(); i++) {
				purse.add(money.get(i));
			}

			for (int i = purse.size() - 1; i >= 0; i--) {

				if (amount >= purse.get(i).getValue()) {
					newPurse.add(purse.get(i));
					amount -= purse.get(i).getValue();

				}
			}

		}
		return newPurse;

	}

}
