package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;

/**
 * Its a class that withdraw that have a recursive way.
 * 
 * @author Narut Poovorakit
 * @version 22.02.2015
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy {

	/**
	 * class that withdraw the money in the purse.
	 * 
	 * @return if the list is null its will process the recursive.
	 */
	public ArrayList<Valuable> withdraw(double amount, List<Valuable> money) {
		// System.out.println("withdraw");
		ArrayList<Valuable> returnList = withdrawHelper(amount, money, 0);

		return returnList;

	}

	/**
	 * A helper to use in recursion.
	 * 
	 * @param amount
	 *            of money.
	 * @param money
	 *            in the purse.
	 * @param index
	 *            of the list.
	 * @return null if it can't grab the money anymore. null if it's grab too
	 *         much. return list of money that been withdraw.
	 */
	public ArrayList<Valuable> withdrawHelper(double amount,
			List<Valuable> money, int index) {

		if (index == money.size()) // Can't grab anymore.
			return null;
		if (amount < 0) // Grab too much.
			return null;
		amount -= money.get(index).getValue();
		if (amount == 0) { // Grab ok that amount want.
			ArrayList<Valuable> needList = new ArrayList<Valuable>();
			needList.add(money.get(index));
			return needList;
		}

		// create a new list that contain a recursion thing.
		ArrayList<Valuable> wantList = withdrawHelper(amount, money, index + 1);
		if (wantList == null) {
			amount += money.get(index).getValue();
			return withdrawHelper(amount, money, index + 1);
		} else {
			wantList.add(money.get(index));
			return wantList;
		}
	}

}
