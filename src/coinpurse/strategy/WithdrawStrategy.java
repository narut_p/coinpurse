package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;

/**
 * 
 * @author Narut Poovorakit
 * @version 17.02.2015
 *
 */
public interface WithdrawStrategy {
	/**
	 * 
	 * @param amount
	 * @param valuables is the list.
	 * 
	 */
	public ArrayList<Valuable> withdraw(double amount, List<Valuable> valuables);

}
