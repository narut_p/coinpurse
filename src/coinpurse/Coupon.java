package coinpurse;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Narut Poovorakit
 *
 */

public class Coupon extends AbstractValuable {
	private String color;
	private Map<String, Double> map;

	/**
	 * Construct a coupon object.
	 * 
	 * @param color
	 *            is a color of the coupon.
	 */
	public Coupon(String color) {
		this.color = color;
		map = new HashMap<String, Double>();
		map.put("red", 100.0);
		map.put("blue", 50.0);
		map.put("green", 20.0);
	}

	public double getValue() {
		return map.get(color);
	}

	/**
	 * @return A color of a coupon.
	 */
	public String toString() {
		return color + " coupon.";
	}

}
