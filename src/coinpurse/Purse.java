package coinpurse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;

/**
 * A purse contains coins, coupon and banknote. You can insert money, withdraw
 * money, check the balance, and check if the purse is full. When you withdraw
 * money, the coin purse decides which coins to remove.
 * 
 * @author Narut Poovorakit
 * @version 27.01.2015
 */
public class Purse extends Observable {
	/** Collection of valuable in the purse. */

	private List<Valuable> money;
	private WithdrawStrategy strategy;
	/**
	 * Capacity is maximum NUMBER of coins the purse can hold. Capacity is set
	 * when the purse is created.
	 */
	private int capacity;

	/**
	 * Create a purse with a specified capacity.
	 * 
	 * @param capacity
	 *            is maximum number of all valuable you can put in purse.
	 */
	public Purse(int capacity) {
		this.capacity = capacity;
		money = new ArrayList<Valuable>();
		strategy = new GreedyWithdraw();
	}

	/**
	 * Count and return the number of all valuable in the purse. This is the
	 * number of all valuable, not their value.
	 * 
	 * @return the number of all valuable in the purse
	 */
	public int count() {
		return money.size();
	}

	/**
	 * Get the total value of all items in the purse.
	 * 
	 * @return the total value of items in the purse.
	 */
	public double getBalance() {
		double sum = 0;
		if (money.size() > 0) {
			for (int i = 0; i < money.size(); i++) {
				sum += money.get(i).getValue();
			}
		}
		return sum;
	}

	/**
	 * Return the capacity of the purse.
	 * 
	 * @return the capacity
	 */

	public int getCapacity() {
		return this.capacity;
	}

	/**
	 * Test whether the purse is full. The purse is full if number of items in
	 * purse equals or greater than the purse capacity.
	 * 
	 * @return true if purse is full.
	 */
	public boolean isFull() {
		if (getCapacity() == money.size()) {
			return true;
		}
		return false;
	}

	/**
	 * Insert a coin, banknote and coupon into the purse. All valuable are only
	 * inserted if the purse has space for it.
	 * 
	 * @param Valuable
	 *            is a valuable object to insert into purse
	 * @return true if valuable inserted, false if can't insert
	 */
	public boolean insert(Valuable coin) {
		if (isFull()) {
			return false;
		} else {
			money.add(coin);
			super.setChanged();
			super.notifyObservers(this);
			return true;
		}

	}

	/**
	 * Withdraw the requested amount of money. Return an array of Valuable
	 * withdrawn from purse, or return null if cannot withdraw the amount
	 * requested.
	 * 
	 * @param amount
	 *            is the amount to withdraw
	 * @return array of Valuable objects for money withdrawn, or null if cannot
	 *         withdraw requested amount.
	 */
	public Valuable[] withdraw(double amount) {

		ArrayList<Valuable> arrayList = new ArrayList<Valuable>();
		WithdrawStrategy withdraw = new RecursiveWithdraw();
		Collections.sort(money, new ValueComparator());
		arrayList = withdraw.withdraw(amount, money);
		if (arrayList == null)
			return null;
		Valuable[] array = new Valuable[arrayList.size()];

		for (int i = 0; i < arrayList.size(); i++) {
			array[i] = arrayList.get(i);
		}

		for (int i = 0; i < arrayList.size(); i++) {
			for (int j = 0; j < money.size(); j++) {
				if (money.get(j).getValue() == arrayList.get(i).getValue()) {
					money.remove(j);
					break;
				}
			}
		}
		super.setChanged();
		super.notifyObservers(this);
		return array;
	}

	public void setWithdrawStrategy(WithdrawStrategy newStrategy) {
		this.strategy = newStrategy;
	}

	/**
	 * toString returns a string description of the purse contents. It can
	 * return whatever is a useful description.
	 */
	public String toString() {
		String str = "";
		for (int i = 0; i < money.size(); i++) {
			str += money.get(i) + " ";
		}

		return "The money in the purse " + str;
	}

}
