package coinpurse;

/**
 * 
 * @author Narut Poovorakit
 * @version 27.01.2015
 */
public class BankNote extends AbstractValuable {
	/**
	 * serial number is start at 1,000,000.
	 */
	private static double serialNumberCount = 1000000;
	private double value;
	private double serialNumber;
	private String currency;

	/**
	 * @param the
	 *            value of the banknote. Construct a banknote.
	 */
	public BankNote(double value, String currency) {
		this.value = value;
		this.currency = currency;
		serialNumber = serialNumberCount;
		getSerialNumberCount();
	}

	/**
	 * 
	 * @return a count of round that been use.
	 */
	public double getSerialNumberCount() {
		serialNumberCount++;
		return serialNumberCount;
	}

	/**
	 * @return return a value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @return A value of banknote with its own serial number.
	 */
	public String toString() {
		return (int)this.value + " " + this.currency + " " + "Banknote";
	}

}
