package coinpurse;
/**
 * 
 * @author Narut Poovorakit
 * @version 10.02.2015
 *
 */
public abstract class AbstractValuable implements Valuable {
	/**
	 * compare the value.
	 * @return whether if the value is more, equal or less.
	 * @param valuable of each such as Coin, BankNote and Coupon.
	 */
	public int compareTo(Valuable valuable) {
			
		if (this.getValue() < valuable.getValue()) {
			return -1;
		}
		if (this.getValue() > valuable.getValue()) {
			return 1;
		}
		else {
			return 0;
		}
		
	}
	/**
	 * @param obj is a object
	 * @return false if obj is null or not a specific valuable.
	 */
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj.getClass() != this.getClass()) {
			return false;
		}
		Valuable val = (Valuable) obj;
		
		return this.getValue() == val.getValue();
	}

}
