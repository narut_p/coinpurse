package coinpurse;

import java.util.ResourceBundle;

/**
 * A class of money factory that create money in various kind of currency.
 * @author Narut Poovorakit
 *
 */
public abstract class MoneyFactory {
	private static MoneyFactory mf;

	static MoneyFactory getInstance() {
		setMoneyFactory();
		return mf;
	}

	abstract Valuable createMoney(double value);

	/**
	 * Set currency of a money.
	 */
	public static void setMoneyFactory() {
		ResourceBundle rb = ResourceBundle.getBundle("bundle");
		String name = rb.getString("fac");
		try {
			mf = (MoneyFactory)Class.forName(name).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			mf = new MalayMoneyFactory();
			e.printStackTrace();
		}
		
		
	}
	
	/**
	 * 
	 * @param value is a value of money
	 * @return money
	 */
	Valuable createMoney(String value) {
		double price = Double.parseDouble(value);
		return mf.createMoney(price);
	}


}
