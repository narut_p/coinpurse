package coinpurse;

/**
 * A coin with a monetary value. You can't change the value of a coin.
 * 
 * @author Narut Poovorakit
 * @version 27.01.2015
 */
public class Coin extends AbstractValuable {

	/** Value of the coin */
	private double value;
	private String currency;
	/**
	 * Constructor for a new coin.
	 * 
	 * @param value
	 *            is the value for the coin
	 */
	public Coin(double value, String currency) {
		this.value = value;
		this.currency = currency;
	}

	/**
	 * return value of coin.
	 * 
	 * @return the value of coin
	 */
	public double getValue() {
		return this.value;
	}

	

	/**
	 * return the value in Baht.
	 */
	public String toString() {
		return (int)this.value + " " + this.currency + " " + "Banknote";
	}

	

}
