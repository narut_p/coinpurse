package coinpurse;

/**
 * The main class
 * 
 * 
 * @author Narut Poovorakit
 * @version
 */
public class Main {
	private static int CAPACITY = 35;

	/**
	 * main method
	 * 
	 */
	public static void main(String[] args) {
		// create an purse object.
		Purse purse = new Purse(CAPACITY);

		BalanceObserver balanceOb = new BalanceObserver();
		balanceOb.run();
		purse.addObserver(balanceOb);
		StatusObserver statusOb = new StatusObserver();
		statusOb.run();
		purse.addObserver(statusOb);

		ConsoleDialog console = new ConsoleDialog(purse);
		console.run();
	}
}
