package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A gui purse balance.
 * 
 * @author Narut Poovorakit
 * @version 24.02.2015
 *
 */
public class BalanceObserver extends JFrame implements Observer {

	// create frame, panel and label.
	private JFrame frame;
	private JPanel pane;
	private JLabel label;

	/**
	 * initialize a new frame.
	 */
	public BalanceObserver() {
		super("Purse Balance");
		frame = new JFrame();
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);

	}

	/**
	 * A method that start. set the layout to be a boxlayout.
	 */
	public void initialize() {
		frame.setSize(200, 200);
		pane = new JPanel();
		// set layout
		pane.setLayout(new BoxLayout(pane, BoxLayout.X_AXIS));
		label = new JLabel("0.00 Baht");

		// Add
		pane.add(label);
		super.add(pane);
	}

	/**
	 * Set the frame to be visible.
	 */
	public void run() {
		this.initialize();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * Create an object purse and set the balance process.
	 */
	public void update(Observable subject, Object Info) {
		if (subject instanceof Purse) {
			Purse purse = (Purse) subject;
			int balance = (int) purse.getBalance();
			label.setText("Current balance: " + balance);

		}
		if (Info != null) {
			System.out.println(Info);
		}

	}
}
