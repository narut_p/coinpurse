package coinpurse;

import java.util.Comparator;

public class ValueComparator implements Comparator<Valuable> {
	/**
	 * comparing value 1 and value2
	 */
	public int compare(Valuable val1, Valuable val2) {
		if (val1.getValue() > val2.getValue()) {
			return 1;
		}
		if (val2.getValue() > val1.getValue()) {
			return -1;
		} else {
			return 0;
		}
	}
}
