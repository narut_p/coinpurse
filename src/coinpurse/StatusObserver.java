package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * The GUI of status bar.
 * 
 * @author Narut Poovorakit
 * @version 24.02.2015
 *
 */
public class StatusObserver extends JFrame implements Observer {
	// create an instance variables.
	private JFrame frame;
	private JProgressBar bar;
	private JPanel pane;
	private JLabel label;

	/**
	 * Create a constructor.
	 */
	public StatusObserver() {
		super("Purse Status");
		frame = new JFrame();
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(200, 200);

	}

	/**
	 * Set all of the panel label and status bar.
	 */
	public void initialize() {
		pane = new JPanel();
		// set layout
		label = new JLabel();
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		bar = new JProgressBar();

		// Add
		pane.add(label);
		pane.add(bar);
		super.add(pane);
	}

	/**
	 * A method that set the visible of the screen to be seen.
	 */
	public void run() {
		this.initialize();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * update the purse, and set the bar.
	 * 
	 * @return if the purse is full, its will show full if count is zero its
	 *         show empty
	 * 
	 */
	public void update(Observable subject, Object Info) {
		if (subject instanceof Purse) {
			Purse purse = (Purse) subject;
			bar.setMaximum(purse.getCapacity());
			bar.setValue(purse.count());

			if (purse.isFull()) {
				label.setText("Full");
			} else if (purse.count() == 0) {
				label.setText("Empty");
			} else
				label.setText(purse.count() + "");

		}
		if (Info != null) {
			System.out.println(Info);
		}

	}
}
